function isValidDate(date) {
    return date && Object.prototype.toString.call(date) === "[object Date]" && !isNaN(date) && new Date(date).toString() !== "Invalid Date";
}

const dateFunctions = {

    getTimeZone: (inputDate) => {
        const whetherDateIsValid = isValidDate(inputDate);
        if(!whetherDateIsValid){
            throw new Error("Invalid Date");
        }
        const rehydratedDate = new Date(inputDate);
        return Intl.DateTimeFormat(rehydratedDate).resolvedOptions().timeZone;
    },

    getLocalDate: (inputDate = new Date()) => {
        const whetherDateIsValid = isValidDate(inputDate);
        if(!whetherDateIsValid){
            throw new Error("Invalid Date");
        }
        const rehydratedDate = new Date(inputDate);
        return new Date(rehydratedDate).toLocaleDateString();
    },

    getLocalTime: (inputDate = new Date()) => {
        const whetherDateIsValid = isValidDate(inputDate);
        if(!whetherDateIsValid){
            throw new Error("Invalid Date");
        }
        const rehydratedDate = new Date(inputDate);
        return new Date(rehydratedDate).toLocaleTimeString();
    },

    getLocalDateTime: (inputDate = new Date()) => {
        const whetherDateIsValid = isValidDate(inputDate);
        if(!whetherDateIsValid){
            throw new Error("Invalid Date");
        }
        const rehydratedDate = new Date(inputDate);
        return new Date(rehydratedDate).toLocaleString();
    },

    getUTCDate: (inputDate) => {
        const whetherDateIsValid = isValidDate(inputDate);
        if(!whetherDateIsValid){
            throw new Error("Invalid Date");
        }
        const rehydratedDate = new Date(inputDate);
        return new Date(rehydratedDate).toUTCString();
    },

    convertToSpecificTimeZone: (inputDate, timeZone) => {
        const whetherDateIsValid = isValidDate(inputDate);
        if(!whetherDateIsValid){
            throw new Error("Invalid Date");
        }
        const rehydratedDate = new Date(inputDate);
        return new Date(rehydratedDate).toLocaleString('en-US', { timeZone: timeZone });
    },

    compareDates: (inputDate1, inputDate2, timeZone1 = "Asia/Kolkata", timeZone2 = "Asia/Kolkata") => {
        let date1, date2;

        if(typeof inputDate1 === "string") {
            date1 = new Date(inputDate1).toLocaleString('en-US', { timeZone: timeZone1 });
        }else {
            date1 = new Date(inputDate1);
        }

        if(typeof inputDate2 === "string") {
            date2 = new Date(inputDate2).toLocaleString('en-US', { timeZone: timeZone2 });
        } else {
            date2 = new Date(inputDate2);
        }

        if(date1.toString() === "Invalid Date" || date2.toString() === "Invalid Date") {
            throw new Error("One or more dates are invalid.");
        }

        const leveledDate1 = new Date(date1).toISOString();
        const date1Seconds = new Date(leveledDate1).getTime() / 1000;
        const leveledDate2 = new Date(date2).toISOString();
        const date2Seconds = new Date(leveledDate2).getTime() / 1000;
        const minutesDifference = (date1Seconds - date2Seconds)/60;
        const hoursDifference = minutesDifference/60;

        const dayDifference = hoursDifference/24;

        return {
            result: leveledDate1 > leveledDate2 ? "Date 1 is greater than Date 2" : leveledDate1 < leveledDate2 ? "Date 1 is less than Date 2" : "Date 1 is equal to Date 2",
            dayDifference: (dayDifference >= 0) ? Math.floor(dayDifference ): Math.ceil(dayDifference) * -1,
            minutesDifference: (minutesDifference >= 0) ? Math.floor(minutesDifference % 60 ): Math.ceil(minutesDifference % 60) * -1,
            hoursDifference: (hoursDifference >= 0) ? Math.floor(hoursDifference % 24 ): Math.ceil(hoursDifference % 24) * -1,
        }
    }



}

export default dateFunctions;